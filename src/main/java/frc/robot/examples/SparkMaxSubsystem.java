package frc.robot.examples;

import com.revrobotics.CANSparkLowLevel;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkBase.IdleMode;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class SparkMaxSubsystem extends SubsystemBase{
    private RelativeEncoder sparkMaxEncoder;
    private CANSparkMax sparkMax;

    public SparkMaxSubsystem() {
        // This example uses a brushed motor with only red and black wires
        sparkMax = new CANSparkMax(ExampleConstants.SPARK_MAX_ID, CANSparkLowLevel.MotorType.kBrushed);
        sparkMax.setIdleMode(IdleMode.kCoast);
    }

    public void setMainSpeed(double speed){
        sparkMax.set(speed * ExampleConstants.SPARK_MAX_SPEED);
    }

    public void stop() {
        sparkMax.set(0);
    }

    public double getMainEncoderAngle() {
        return sparkMaxEncoder.getPosition(); // need to do some math here * Constants.ARM_MAIN_ENCODER_CONVERSION;
    }

    public double getMainEncoderPosition() {
        return sparkMaxEncoder.getPosition();
    }

    public void zeroEncoders() {
        sparkMaxEncoder.setPosition(0);
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Spark Max Motor Speed ", sparkMaxEncoder.getVelocity());
        SmartDashboard.putNumber("Spark Max Encoder Position ", getMainEncoderPosition());
    }

}
