package frc.robot.examples;

import edu.wpi.first.wpilibj2.command.Command;
import frc.robot.RobotContainer;


// turn on shooter and set to certain speed
public class ButtonSparkMaxCommand extends Command{
  
  public ButtonSparkMaxCommand() {
    addRequirements(RobotContainer.sparkMaxSubsystem);
  } 

  public void execute(){
    RobotContainer.sparkMaxSubsystem.setMainSpeed(-1.0);

  }
  
  @Override
  public void end(boolean interrupted){
    RobotContainer.sparkMaxSubsystem.stop();
  }
}
