package frc.robot.examples;

public class ExampleConstants {
    // Spark Max Constants
    public static final int SPARK_MAX_ID = 11;      // CAN ID
    public static final double SPARK_MAX_SPEED = 1.0;
    
}
